//CONSTANTS
Main.prototype.GAME_INITED =			0;
Main.prototype.GAME_RESET =				1;
Main.prototype.GAME_PLAYING =			2;
Main.prototype.GAME_OVER =				3;
Main.prototype.GAME_PAUSED =			4;
Main.prototype.GAME_DISORIENTED =		5;
Main.prototype.DEFAULT_CANVAS_WIDTH =	800;
Main.prototype.DEFAULT_CANVAS_HEIGHT =	450;
Main.prototype.NUM_NPC_ROWS =			2;
Main.prototype.NUM_NPCS_PER_ROW =		5;
Main.prototype.NPC_ROW_START =			-430;	//the y-coordinate of the first row of NPCs
Main.prototype.NPC_ROW_MARGIN =			430;	//number of pixels between each row of NPCs
Main.prototype.TRACK_LEFT =				120;	//far left side of the actual race track on the background
Main.prototype.TRACK_RIGHT =			Main.prototype.DEFAULT_CANVAS_WIDTH - 120;
Main.prototype.TRACK_WIDTH =			Main.prototype.TRACK_RIGHT - Main.prototype.TRACK_LEFT;
Main.prototype.BG_SPEED =				2.5;

//PRIMITIVES
Main.prototype._canvasWidth;
Main.prototype._canvasHeight;
Main.prototype._isMuted;
Main.prototype._currentScore = 0;
Main.prototype._highScore = 0;
Main.prototype._gameState = 0;
Main.prototype._prevGameState = 0;
Main.prototype._currentEmptySlotIndex = 0;
Main.prototype._inputX = 0;

//OBJECTS
Main.prototype._worldContainer;
Main.prototype._uiManager;
Main.prototype._bg1;
Main.prototype._bg2;
Main.prototype._pc;
Main.prototype._npcs = [];
Main.prototype._crash;
Main.prototype._music;


//=================================
//		SETUP
//=================================

function Main() {
	this.StartInitState();
}


//=================================
//		STATE MANAGEMENT
//=================================

/**
* Starts the initialization state, which the game will start in. In this state, all initially relevant objects are created.
* 
* FROM: None. Default state.
* TO: Reset State, when all initially relevant assets are created.
*/
Main.prototype.StartInitState = function() {
	console.log("Main.StartInitState()");
	
	this._prevGameState = this._gameState = this.GAME_INITED;
	
	this._bg1 = _game.add.sprite(0, 0, 'bg');
	this._bg2 = _game.add.sprite(0, 0, 'bg');
	
	var newNPC;
	for (var i = 0; i < this.NUM_NPC_ROWS; i++)
	{
		var row = [];
		for (var j = 0; j < this.NUM_NPCS_PER_ROW; j++)
		{
			newNPC = new NPC();
			
			row[j] = newNPC;
		}
		
		this._npcs[i] = row;
	}
	
	this._pc = new PC();
	
	//Retrieve locally stored pref: mute
	//this._isMuted = this.GetCookie("bGPGMuted") == "1";

	//if (this._isMuted)
	//{
	//	this.MuteGame();
	//}
	//else
	//{
	//	this.UnmuteGame();
	//}

	//Create the sound effects
	//this._crash = createjs.Sound.createInstance("car_crash");
	//this._music = createjs.Sound.createInstance("music");
	//this._music.loop = -1;

	//Retrieve locally stored high score
	//this._highScore = parseInt(this.GetCookie("iGPGHighScore"));

	//if (isNaN(this._highScore))	//set it to 0 if no high score or an invalid high score was retrieved
	//{
	//	console.log("Main.BuildWorld(): No high score or invalid high score retrieved. Setting high score to 0!");
	//
	//	this._highScore = 0;
	//
	//	this.SetCookie("iGPGHighScore", "0");
	//}
	
	//UI
	this._uiManager = new UIManager();
	this._uiManager.startInitState(this);
	
	this.StartResetState();
};

/**
* Starts the reset state. In this state, the score and all game objects are returned to their starting points and the player is prompted to start the game.
* 
* FROM: Init State, when all initially relevant objects are created.
* FROM: Game Over State, when the player loses the game.
* TO: Paused State, when all game objects are returned to their starting points.
*/
Main.prototype.StartResetState = function() {
	console.log("Main.StartResetState()");
	
	this._prevGameState = this._gameState;
	this._gameState = this.GAME_RESET;
	
	//Reset game objects
	this._currentScore = 0;
	
	this._bg1.y = 0;
	this._bg2.y = -this.DEFAULT_CANVAS_HEIGHT;
	
	this._pc.reset();
	
	for (var i = 0; i < this._npcs.length; i++)
	{
		this.ResetNPCRow(this._npcs[i]);

		//The first row's y-coordinate is set manually. Subsequent rows will fall in behind it
		if (i == 0)
		{
			for (var j = 0; j < this._npcs[i].length; j++)
			{
				this._npcs[i][j].view.y = this.NPC_ROW_START;
			}
		}
	}

	//UI
	this._uiManager.startResetState();

	this.StartPausedState();
};

/**
* Starts the paused state. In this state, the game loop stops and the player has no input options except to unpause the game. 
* If the device is in portrait mode upon entering the paused state, the game will immediately switch to disoriented state.
* 
* FROM: Reset State, when all game objects are returned to their starting points.
* FROM: Playing State, when the player pauses the game.
* FROM: Disoriented State, when the player turns the mobile device to landscape mode.
* TO: Playing State, when the player unpauses the game.
* TO: Disoriented State, if the mobile device is in portrait mode.
*/
Main.prototype.StartPausedState = function() {
	console.log("Main.StartPausedState()");
	
	this._prevGameState = this._gameState;
	this._gameState = this.GAME_PAUSED;

	//Pause music
	//_music.paused = true;

	//Clear user input
	this._inputX = 0;

	//UI
	this._uiManager.startPausedState();
};

/**
* Starts the disoriented state. In this state, the game loop stops and the player has no input options except to turn the mobile device to landscape mode.
* 
* FROM: Paused State, if the mobile device is in portrait mode.
* TO: Paused State, when the player turns the mobile device to landscape mode.
*/
Main.prototype.StartDisorientedState = function() {
	console.log("Main.StartDisorientedState()");

	//UI
	this._uiManager.startDisorientedState();
};

/**
* Starts the playing state. In this state, the player controls the PC and plays the game.
* 
* FROM: Reset State, when the player starts the game.
* FROM: Paused State, when the player unpauses the game.
* TO: Paused State, when the player pauses the game or turns the mobile device to portrait mode.
* TO: Game Over State, when the player loses the game.
*/
Main.prototype.StartPlayingState = function() {
	console.log("Main.StartPlayingState()");

	this._prevGameState = this._gameState;
	this._gameState = this.GAME_PLAYING;

	//Turn on music
	//this._music.paused = false;
	//this._music.play();
	
	//UI
	this._uiManager.startPlayingState();
};

/**
* Starts the game over state. In this state, the player has no input options until the death animation is complete, after which the game will enter the reset state.
* 
* FROM: Playing state, when the player loses the game.
* TO: Reset State, when the PC death animation is complete.
*/
Main.prototype.StartGameOverState = function() {
	console.log("Main.StartGameOverState()");

	this._prevGameState = this._gameState;
	this._gameState = this.GAME_OVER;
	
	//Update high score
	if (this._currentScore > this._highScore)
	{
		this._highScore = this._currentScore;
		
		this.SetCookie("iGPGHighScore", this._highScore.toString());
	}
	
	//Turn off music
	//this._music.paused = false;
	//this._music.stop();

	//Clear user input
	this._inputX = 0;
	
	//Wait for the PC's death to finish animating
	setTimeout( () => this.HandleGameEnded(), 1500 );

	//UI
	this._uiManager.startGameOverState();
};


//=================================
//		GAME LOOP
//=================================

Main.prototype.GameLoop = function() {
	if (this._gameState == this.GAME_PLAYING)
	{
		this.UpdateWorld();
		this.UpdatePC();
		this.UpdateNPCs();
		this.DetectCollisions();
	}
	else if (this._gameState == this.GAME_OVER)
	{
		this.UpdatePC();
	}
	
	this.UpdateUI();
};

Main.prototype.UpdateWorld = function() {
	this._bg1.y += this.BG_SPEED;
	this._bg2.y += this.BG_SPEED;

	//Flip flop BG
	if (this._bg1.y > this.DEFAULT_CANVAS_HEIGHT)
	{
		this._bg1.y = this._bg2.y - this._bg1.height;
	}
	else if (this._bg2.y > this.DEFAULT_CANVAS_HEIGHT)
	{
		this._bg2.y = this._bg1.y - this._bg2.height;
	}
};

Main.prototype.UpdatePC = function() {
	this._pc.update(this._inputX);
};

Main.prototype.UpdateNPCs = function() {
	for (var i = 0; i < this._npcs.length; i++)
	{
		for (var j = 0; j < this._npcs[i].length; j++)
		{
			this._npcs[i][j].update();
		}
	}
};

Main.prototype.DetectCollisions = function() {
	//PC VS. WALLS
	var pcLeft = this._pc.view.x;
	var pcRight = pcLeft + this._pc.view.width;
	if (pcLeft < this.TRACK_LEFT || pcRight > this.TRACK_RIGHT)
	{
		this._pc.view.x -= this._pc.dx;
		this._pc.dx = -this._pc.dx * .9;
	}
	
	//PC VS. NPCS
	var thisNPC;
	pcVnpc: for (var i = 0; i < this._npcs.length; i++)
	{
		for (var j = 0; j < this._npcs[i].length; j++)
		{
			thisNPC = this._npcs[i][j];

			if (this.IsColliding(this._pc, thisNPC))
			{
				//this._crash.play();

				this._pc.kill();

				this.StartGameOverState();

				break pcVnpc;
			}
		}
	}
	
	//NPCS VS. LEVEL
	npcVlevel: for (var i = 0; i < this._npcs.length; i++)
	{
		for (var j = 0; j < this._npcs[i].length; j++)
		{
			thisNPC = this._npcs[i][j];

			if (thisNPC.collisionBox.y > this.DEFAULT_CANVAS_HEIGHT + 30)	//+30 to account for y variations within row
			{
				this.ResetNPCRow(this._npcs[i]);

				this._currentScore++;
				
				break npcVlevel;
			}
		}
	}
};

Main.prototype.IsColliding = function(char1, char2) {
	if (char1.collisionBox.x > char2.collisionBox.x + char2.collisionBox.width || char1.collisionBox.x + char1.collisionBox.width < char2.collisionBox.x)
	{
		return false;
	}

	if (char1.collisionBox.y > char2.collisionBox.y + char2.collisionBox.height || char1.collisionBox.y + char1.collisionBox.height < char2.collisionBox.y)
	{
		return false;
	}

	return true;
};

Main.prototype.UpdateUI = function() {
	this._uiManager.update(this._currentScore, this._highScore, this._gameState == this.GAME_PAUSED, this._isMuted);
};


//=================================
//		UI MANAGEMENT
//=================================

//======	Device Orientation

Main.prototype.handleUserChangedOrientation = function() {
	this.StartPausedState();
}

//======	Mouse > World

Main.prototype.handleUserMousedDownWorld = function(localMouseX) {
	if (localMouseX < this.DEFAULT_CANVAS_WIDTH / 2)
	{
		this._inputX = -1;
	}
	else
	{
		this._inputX = 1;
	}
};

Main.prototype.handleUserMousedUpWorld = function() {
	this._inputX = 0;
};

Main.prototype.handleUserClickedWorld = function() {
	this.StartPlayingState();
};

//======	Mouse > Pause Button

/**
* Toggles game pause.
*
* @return Returns true if the game is now paused, false if not.
*/
Main.prototype.handleUserClickedPauseButton = function() {
	//Toggle pause
	if (this._gameState == this.GAME_PAUSED)
	{
		this.StartPlayingState();

		return false;
	}
	else
	{
		this.StartPausedState();

		return true;
	}
};

//======	Mouse > Mute Button

/**
* Toggles game mute.
*
* @return Returns true if the game is now muted, false if not.
*/
Main.prototype.handleUserClickedMuteButton = function() {
	//Toggle and save mute state
	if (this._isMuted)
	{
		this.UnmuteGame();

		return false;
	}
	else
	{
		this.MuteGame();

		return true;
	}
};


//=================================
//		MISC.
//=================================

Main.prototype.MuteGame = function() {
	this._isMuted = true;

	createjs.Sound.setMute(true);
	
	this.SetCookie("bGPGMuted", "1");
}

Main.prototype.UnmuteGame = function() {
	this._isMuted = false;

	createjs.Sound.setMute(false);
	
	this.SetCookie("bGPGMuted", "0");
}


//=================================
//		COOKIES
//=================================

Main.prototype.GetCookie = function(cookieName) {
	//console.log("Main.GetCookie(): document.cookie = " + document.cookie);

	var name = cookieName + "=";
	var ca = document.cookie.split(';');
	for (var i = 0; i < ca.length; i++)
	{
		var cookie = ca[i];
		while (cookie.charAt(0) == " ") cookie = cookie.substring(1);
		if (cookie.indexOf(name) == 0) return cookie.substring(name.length, cookie.length);
	}

	return "";
}

Main.prototype.SetCookie = function(cookieName, cookieValue) {
	document.cookie = cookieName + "=" + cookieValue + "; expires=Fri, 31 Dec 9999 23:59:59 GMT";

	//console.log("Main.SetCookie(): document.cookie = " + document.cookie);
}


//=================================
//		MISC.
//=================================

Main.prototype.ResetNPCRow = function(row) {
	//Randomly place an empty slot amidst the cars in this row
	var numSlots = this.NUM_NPCS_PER_ROW + 1;	//+1 to account for the empty slot
	var emptySlotIndex;
	do
	{
		emptySlotIndex = Math.floor(Math.random() * numSlots);
	}
	while (emptySlotIndex == this._currentEmptySlotIndex);
	this._currentEmptySlotIndex = emptySlotIndex;
	
	//Update coordinates
	var thisNPC;
	for (var i = 0; i < numSlots; i++)
	{
		if (i == emptySlotIndex) 
		{
			continue;
		}
		else
		{
			if (i > emptySlotIndex)
			{
				thisNPC = row[i - 1];
			}
			else
			{
				thisNPC = row[i];
			}

			var marginWidth = (this.TRACK_WIDTH - (numSlots * thisNPC.view.width)) / (numSlots + 1);
			var segmentWidth = marginWidth + thisNPC.view.width;
			var thisNPCX = this.TRACK_LEFT + segmentWidth * i + marginWidth;
			
			thisNPC.reset();
			thisNPC.view.x = thisNPCX;
		}
	}

	//Update y-coordinate: find the row in front of this one, then put this row above that row on the screen
	var thisRowIndex = this._npcs.indexOf(row);
	var nextRowIndex = (thisRowIndex == 0) ? this._npcs.length - 1 : thisRowIndex - 1;
	for (i = 0; i < row.length; i++)
	{
		thisNPC = row[i];

		if (this._npcs.length == 1)
		{
			thisNPC.view.y = -NPC_ROW_MARGIN;
		}
		else
		{
			thisNPC.view.y = this._npcs[nextRowIndex][0].view.y - this.NPC_ROW_MARGIN;
		}
	}
};

Main.prototype.HandleGameEnded = function() {
	this.StartResetState();
};