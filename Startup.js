var _game;
var _main;

function Startup() {
	_game = new Phaser.Game(800, 450, Phaser.AUTO, '', { preload: preload, create: create, update: update });
}

function preload() {
	_game.load.image('bg', 'assets/bg.jpg');
	_game.load.image('pc_dead', 'assets/pc_dead.png');
	_game.load.spritesheet('mute_button', 'assets/mute_button.png', 64, 64);
	_game.load.spritesheet('npc', 'assets/npc.png', 80, 110);
	_game.load.spritesheet('pc', 'assets/pc.png', 80, 110);
}

function create() {
	_game.input.mouse.capture = true;
	_game.time.advancedTiming = true;
	
	_main = new Main();
}

function update() {
	_main.GameLoop();
	//_game.debug.text(_game.time.fps, 2, 14, "#00ff00");
}