﻿//OBJECTS
UIManager.prototype._main;
UIManager.prototype._guiContainer;
UIManager.prototype._guiBounds;
UIManager.prototype._txtCurrentScore;
UIManager.prototype._txtHighScore;
UIManager.prototype._txtStartPrompt;
UIManager.prototype._pauseButton;
UIManager.prototype._muteButton;


//=================================
//		SETUP
//=================================

function UIManager() {
	
}


/**
* Notifies the UI of all relevant, up-to-date information to show the user.
*/
UIManager.prototype.update = function(currentScore, highScore, isPaused, isMuted) {
	//Current score text field
	this._txtCurrentScore.text = "Score: " + currentScore;
	this._txtCurrentScore.x = 800 - this._txtCurrentScore.width - 10;
	
	//High score text field
	this._txtHighScore.text = "High Score: " + highScore;
	
	//Pause button
	//this._pauseButton.gotoAndStop(isPaused ? 0 : 1);
	
	//Mute button
	//this._muteButton.gotoAndStop(isMuted ? 0 : 1);
};


//=================================
//		STATE MANAGEMENT
//=================================

/**
* Starts the initialization state, which the game will start in. In this state, all UI elements are created.
* 
* FROM: None. Default state.
* TO: Reset State, when all initially relevant objects are created.
*
* @param main A reference to the main game object. Required in order to notify it of user interaction. This reference would be replaced with Observer Manager in a larger project.
*/
UIManager.prototype.startInitState = function(main) {
	this._main = main;
	
	//Create the parent object and the bounds that will be used for the UI elements
	this._guiContainer = new Phaser.Group(_game);
	this._guiBounds = new Phaser.Rectangle(0, 0, 800, 450);
	
	//Create the shadow that will be applied to the UI text fields
	//var textShadow: createjs.Shadow = new createjs.Shadow("#000000", 2, 2, 3);
	
	//Current score text field
	this._txtCurrentScore = new Phaser.Text(_game, 0, 0, "Score: N/A");
	//this._txtCurrentScore.shadow = textShadow;
	this._guiContainer.addChild(this._txtCurrentScore);
	
	//High score text field
	this._txtHighScore = new Phaser.Text(_game, 0, 0, "High Score: N/A");
	//this._txtHighScore.shadow = textShadow;
	this._guiContainer.addChild(this._txtHighScore);
	
	//Start prompt text field
	this._txtStartPrompt = new Phaser.Text(_game, 0, 0, "Click to Start");
	//this._txtStartPrompt.shadow = textShadow;
	this._guiContainer.addChild(this._txtStartPrompt);
	
	//Pause button
	/* var pauseButtonSpritesheetData: Object =
		{
			images: [AssetManager.getAssetByID("mute_button")],
			frames: { width: 64, height: 64 }
		};
	var pauseButtonSpriteSheet: createjs.SpriteSheet = new createjs.SpriteSheet(pauseButtonSpritesheetData);
	this._pauseButton = new createjs.Sprite(pauseButtonSpriteSheet);
	this._pauseButton.regX = this._pauseButton.getBounds().width / 2;
	this._pauseButton.regY = this._pauseButton.getBounds().height / 2;
	this._pauseButton.cursor = "pointer";
	this._pauseButton.addEventListener("mouseout", this.HandleUserMousedOutPauseButton);
	this._pauseButton.addEventListener("mousedown", this.HandleUserMousedDownPauseButton);
	this._guiContainer.addChild(this._pauseButton); */

	//Mute button
	/* var muteButtonSpritesheetData: Object =
		{
			images: [AssetManager.getAssetByID("mute_button")],
			frames: { width: 64, height: 64 }
		};
	var muteButtonSpriteSheet: createjs.SpriteSheet = new createjs.SpriteSheet(muteButtonSpritesheetData);
	this._muteButton = new createjs.Sprite(muteButtonSpriteSheet);
	this._muteButton.regX = this._muteButton.getBounds().width / 2;
	this._muteButton.regY = this._muteButton.getBounds().height / 2;
	this._muteButton.cursor = "pointer";
	this._muteButton.addEventListener("mouseout", this.HandleUserMousedOutMuteButton);
	this._muteButton.addEventListener("mousedown", this.HandleUserMousedDownMuteButton);
	this._guiContainer.addChild(this._muteButton); */
};

/**
* Starts the reset state. In this state, all UI elements and user input listeners are set to their default states.
* 
* FROM: Init State, when all initially relevant objects are created.
* FROM: Game Over State, when the player loses the game.
* TO: Paused State, when all game objects are returned to their starting points.
*/
UIManager.prototype.startResetState = function() {
	//Current score text field
	this._txtCurrentScore.x = 800 - this._txtCurrentScore.width - 10;
	this._txtCurrentScore.y = 5;
	this._txtCurrentScore.visible = false;
	
	//High score text field
	this._txtHighScore.x = this._guiBounds.width / 2 - this._txtHighScore.width / 2;	//center
	this._txtHighScore.y = this._guiBounds.height / 2 - this._txtHighScore.height / 2 - 50;	//just above center
	this._txtHighScore.visible = false;
	
	//Start prompt text field
	this._txtStartPrompt.x = this._guiBounds.width / 2 - this._txtStartPrompt.width / 2;
	this._txtStartPrompt.y = this._guiBounds.height / 2 - this._txtStartPrompt.height / 2 + 50;
	this._txtStartPrompt.visible = false;
	
	//Pause button
	/* this._pauseButton.x = this._pauseButton.getBounds().width / 2 + 10;
	this._pauseButton.y = this._pauseButton.getBounds().height / 2 + 10;
	this._pauseButton.removeEventListener("click", this.HandleUserClickedPauseButton);
	this._pauseButton.visible = false; */

	//Mute button
	/* this._muteButton.x = this._pauseButton.x;
	this._muteButton.y = this._pauseButton.y + this._pauseButton.getBounds().height / 2 + 10 + this._muteButton.getBounds().height / 2 + 10;
	this._muteButton.removeEventListener("click", this.HandleUserClickedMuteButton);
	this._muteButton.visible = false; */

	//PC controls
	_game.input.onDown.remove(this.HandleUserMousedDownWorld, this);
	_game.input.onUp.remove(this.HandleUserMousedUpWorld, this);
	
	//World pause toggle
	_game.input.onTap.remove(this.HandleUserClickedWorld, this);
};

/**
* Starts the paused state. In this state, only the appropriate UI elements are shown.
* 
* FROM: Reset State, when all game objects are returned to their starting points.
* FROM: Playing State, when the player pauses the game.
* TO: Playing State, when the player unpauses the game.
*/
UIManager.prototype.startPausedState = function() {
	//Current score text field
	this._txtCurrentScore.visible = true;
	
	//High score text field
	this._txtHighScore.visible = true;
	
	//Start prompt text field
	this._txtStartPrompt.visible = true;
	
	//Pause button
	//this._pauseButton.addEventListener("click", this.HandleUserClickedPauseButton);
	//this._pauseButton.visible = true;

	//Mute button
	//this._muteButton.addEventListener("click", this.HandleUserClickedMuteButton);
	//this._muteButton.visible = true;
	
	//PC controls
	_game.input.onDown.remove(this.HandleUserMousedDownWorld, this);
	_game.input.onUp.remove(this.HandleUserMousedUpWorld, this);
	
	//World pause toggle
	_game.input.onTap.add(this.HandleUserClickedWorld, this);
};

/**
* Starts the disoriented state. In this state, the player has no input options except to turn the mobile device to landscape mode.
* 
* FROM: Paused State, if the mobile device is in portrait mode.
* TO: Paused State, when the player turns the mobile device to landscape mode.
*/
UIManager.prototype.startDisorientedState = function() {
	//Current score text field
	this._txtCurrentScore.visible = false;

	//High score text field
	this._txtHighScore.visible = false;

	//Start prompt text field
	this._txtStartPrompt.visible = false;

	//Pause button
	//this._pauseButton.visible = false;

	//Mute button
	//this._muteButton.visible = false;
	
	//World pause toggle
	_game.input.onTap.remove(this.HandleUserClickedWorld, this);
};

/**
* Starts the playing state. In this state, the player controls the PC and plays the game.
* 
* FROM: Paused State, when the player unpauses the game.
* TO: Paused State, when the player pauses the game or turns the mobile device to portrait mode.
* TO: Game Over State, when the player loses the game.
*/
UIManager.prototype.startPlayingState = function() {
	//High score text field
	this._txtHighScore.visible = false;
	
	//Start prompt text field
	this._txtStartPrompt.visible = false;
	
	//PC controls
	_game.input.onDown.add(this.HandleUserMousedDownWorld, this);
	_game.input.onUp.add(this.HandleUserMousedUpWorld, this);
	
	//World pause toggle
	_game.input.onTap.remove(this.HandleUserClickedWorld, this);
};

/**
* Starts the game over state. In this state, the player has no input options until the death animation is complete, after which the game will enter the reset state.
* 
* FROM: Playing state, when the player loses the game.
* TO: Reset State, when the PC death animation is complete.
*/
UIManager.prototype.startGameOverState = function() {
	//Pause button
	//this._pauseButton.removeEventListener("click", this.HandleUserClickedPauseButton);

	//Mute button
	//this._muteButton.removeEventListener("click", this.HandleUserClickedMuteButton);
	
	//PC controls
	_game.input.onDown.remove(this.HandleUserMousedDownWorld, this);
	_game.input.onUp.remove(this.HandleUserMousedUpWorld, this);
	
	//World pause toggle
	_game.input.onTap.remove(this.HandleUserClickedWorld, this);
};


//=================================
//		USER INPUT
//=================================

//======	Mouse > World

UIManager.prototype.HandleUserMousedDownWorld = function() {
	this._main.handleUserMousedDownWorld(_game.input.x);
};

UIManager.prototype.HandleUserMousedUpWorld = function() {
	this._main.handleUserMousedUpWorld();
};

UIManager.prototype.HandleUserClickedWorld = function() {
	this._main.handleUserClickedWorld();
};

//======	Mouse > Pause Button

UIManager.prototype.HandleUserMousedOutPauseButton = function() {
	this._pauseButton.scaleX = this._pauseButton.scaleY = 1;
};

UIManager.prototype.HandleUserMousedDownPauseButton = function() {
	this._pauseButton.scaleX = this._pauseButton.scaleY = .8;
};

UIManager.prototype.HandleUserClickedPauseButton = function() {
	this._pauseButton.scaleX = this._pauseButton.scaleY = 1;
	
	this._main.handleUserClickedPauseButton();
};

//======	Mouse > Mute Button

UIManager.prototype.HandleUserMousedOutMuteButton = function() {
	this._muteButton.scaleX = this._muteButton.scaleY = 1;
};

UIManager.prototype.HandleUserMousedDownMuteButton = function() {
	this._muteButton.scaleX = this._muteButton.scaleY = .8;
};

UIManager.prototype.HandleUserClickedMuteButton = function() {
	this._muteButton.scaleX = this._muteButton.scaleY = 1;
	
	this._main.handleUserClickedMuteButton();
};