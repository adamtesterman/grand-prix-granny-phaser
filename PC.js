﻿//CONSTANTS
PC.prototype.START_X =				400;
PC.prototype.START_Y =				550;
PC.prototype.END_Y =				450 - 32;
PC.prototype.MAX_DX =				8;	//pixels per frame
PC.prototype.ACCELERATION_X =		.5;
PC.prototype.ACCELERATION_Y =		-2.5;
PC.prototype.DEAD_START_Y =			-500;
PC.prototype.DEAD_START_DY_MIN =	-11;
PC.prototype.DEAD_START_DY_MAX =	-12;
PC.prototype.DEAD_DECAY =			.05;	//how quickly the dead PC decelerates (percent per frame)

//PRIMITIVES
PC.prototype._dx = 0;
PC.prototype._dy = 0;
PC.prototype._deadDX = 0;
PC.prototype._deadDY = 0;
PC.prototype._isVeering;
PC.prototype._isDead;

//OBJECTS
PC.prototype._view;
PC.prototype._viewDead;
PC.prototype._collisionBox;


//=================================
//		SETUP
//=================================

function PC() {
	this.createViews();
	
	this._collisionBox = new Phaser.Rectangle(0, 0, 60, 80);	//position updated later
}


PC.prototype.createViews = function() {
	this._view = _game.add.sprite(0, 0, 'pc');
	
	this._viewDead = _game.add.sprite(0, 0, 'pc_dead');
	this._viewDead.anchor.x = this._viewDead.width / 2;
	this._viewDead.anchor.y = this._viewDead.height / 2;
	this._viewDead.visible = false;
};


PC.prototype.reset = function() {
	this._dx = 0;
	this._dy = 0;
	this._deadDX = 0;
	this._deadDY = 0;
	this._isVeering = false;
	this._isDead = false;

	//this._view.gotoAndStop(0);
	this._view.x = this.START_X - this._view.width / 2;
	this._view.y = this.START_Y;

	this._viewDead.visible = false;
};


PC.prototype.update = function(inputX) {
	if (this._isDead)
	{
		this._viewDead.x += this._deadDX;
		this._viewDead.y += this._deadDY;
		
		//Decelerate
		this._deadDX *= (1 - this.DEAD_DECAY);
		this._deadDY *= (1 - this.DEAD_DECAY);

		if (this._deadDY > -1) 
		{
			this._deadDX = 0;
			this._deadDY = 0;
		}
		else
		{
			this._viewDead.rotation += this._deadDY;
		}
	}
	else
	{
		//UPDATE DX
		//If the player is touching the screen, accelerate PC toward the touch
		if (inputX != 0)
		{
			//Stop veering if needed
			if (this._isVeering)
			{
				this._isVeering = false;
				this._dx = 0;	//so the player doesn't have to fight against the inertia
			}
			
			//If player is pressing left, accelerate left
			if (inputX < 0)
			{
				this._dx -= this.ACCELERATION_X;
			}
			//Otherwise, accelerate right
			else if (inputX > 0)
			{
				this._dx += this.ACCELERATION_X;
			}
		}
		//Otherwise, as long as the PC isn't veering, decelerate
		else if (!this._isVeering)
		{
			if (this._dx < 0)
			{
				this._dx += this.ACCELERATION_X;

				if (this._dx > 0) this._dx = 0;
			}
			else if (this._dx > 0)
			{
				this._dx -= this.ACCELERATION_X;

				if (this._dx < 0) this._dx = 0;
			}
		}
		
		//Bounds check dx
		if (this._dx < -this.MAX_DX)
		{
			this._dx = -this.MAX_DX;
		}
		else if (this._dx > this.MAX_DX)
		{
			this._dx = this.MAX_DX;
		}
		//Start veering if stopped
		else if (this._dx == 0)
		{
			this._isVeering = true;
				
			//Randomly make the PC veer left or right
			if (Math.random() < .5)
			{
				this._dx = -this.ACCELERATION_X;
			}
			else
			{
				this._dx = this.ACCELERATION_X;
			}
		}
		
		//UPDATE DY
		//If PC has reached the ending y-coordinate, no need to move it
		if (this._view.y == this.END_Y - this._view.height)
		{
			this._dy = 0;
		}
		//Otherwise, keep moving PC toward it
		else
		{
			this._dy = this.ACCELERATION_Y;
		}
			
		//UPDATE VIEW
		this._view.x += this._dx;
		this._view.y += this._dy;
			
		//BOUNDS CHECK Y
		if (this._view.y < this.END_Y - this._view.height)
		{
			this._view.y = this.END_Y - this._view.height;
		}
		
		//UPDATE COLLISION BOX
		this._collisionBox.x = this._view.x + 10;
		this._collisionBox.y = this._view.y + 15;
	}
};


PC.prototype.kill = function() {
	this._isDead = true;

	//this._view.gotoAndStop(1);
	
	//Center dead PC on PC
	this._viewDead.x = this._view.x + this._view.width / 2;
	this._viewDead.y = this._view.y + this._view.height / 2;
	this._viewDead.visible = true;
	//this._view.parent.addChild(this._viewDead);
	
	//Randomly determine how quickly the dead PC will start flying forward
	var randDY = Math.random() * (this.DEAD_START_DY_MAX - this.DEAD_START_DY_MIN) + this.DEAD_START_DY_MIN;

	this._deadDX = this._dx;
	this._deadDY = randDY;
	
	//Randomly set start rotation
	var randAngle = Math.random() * 180;
	if (Math.random() < .5) randAngle *= -1;

	this._viewDead.rotation = randAngle;
};


//=================================
//		ACCESSORS
//=================================

Object.defineProperty(PC.prototype, 'collisionBox', {
	get: function() { return this._collisionBox; },
});

Object.defineProperty(PC.prototype, 'dx', {
	get: function() { return this._dx; },
	set: function(value) { this._dx = value }
});

Object.defineProperty(PC.prototype, 'view', {
	get: function() { return this._view; }
});