﻿//CONSTANTS
NPC.prototype.MIN_DY =				3.5;
NPC.prototype.MAX_DY =				4.5;
NPC.prototype.ACCELERATION_Y =		.01;

//PRIMITIVES
NPC.prototype._dx = 0;
NPC.prototype._dy = 0;
NPC.prototype._isAccelerating;

//OBJECTS
NPC.prototype._view;
NPC.prototype._collisionBox;


//=================================
//		SETUP
//=================================

function NPC() {
	this.CreateView();

	this._collisionBox = new Phaser.Rectangle(0, 0, 60, 80);	//positions updated later
}


NPC.prototype.CreateView = function() {
	this._view = _game.add.sprite(0, 0, 'npc');
};


NPC.prototype.reset = function() {
	this._dx = 0;
	this._dy = Math.random() * (this.MAX_DY - this.MIN_DY) + this.MIN_DY;

	var rand = Math.floor(Math.random() * 3);
	//this._view.gotoAndStop(rand);
};


NPC.prototype.update = function() {
	//UPDATE DY
	if (this._isAccelerating)
	{
		this._dy += this.ACCELERATION_Y;
	}
	else
	{
		this._dy -= this.ACCELERATION_Y;
	}
	
	//Bounds check dy
	if (this._dy > this.MAX_DY)
	{
		this._dy = this.MAX_DY;

		this._isAccelerating = false;
	}
	else if (this._dy < this.MIN_DY)
	{
		this._dy = this.MIN_DY;

		this._isAccelerating = true;
	}
	
	//UPDATE VIEW
	this._view.x += this._dx;
	this._view.y += this._dy;
	
	//UPDATE COLLISION BOX
	this._collisionBox.x = this._view.x + 10;
	this._collisionBox.y = this._view.y + 15;
};


//=================================
//		ACCESSORS
//=================================

Object.defineProperty(NPC.prototype, 'collisionBox', {
	get: function() { return this._collisionBox; },
});

Object.defineProperty(NPC.prototype, 'view', {
	get: function() { return this._view; }
});